# Zimmer System

This is the source code for my lamps in my room. Sadly, I don't have any schematics.
This is here is the second software version, compatible with hardware version 4.

Disclaimer: I build (and coded) this almost 2 years ago. I only rewrote part of the code (only to make it look better),
but functionality and the design stayed the same. Please don't judge me:)
## Features
- Multiple switches
- Multiple lamps
- WebServer

## Hardware used
- CPU cooler with 10W LED lamps attached
- PVC pipes
- Tons of cables
- LEDs, Push buttons
- ESP8266
- OLD (slightly modified) PC power supply
- many more things

## Lamps
The lamps are china 10W 12V ones, bought pretty cheap on Amazon. They are glued (with thermal paste) onto cpu coolers.
The cpu coolers are sticked to 2 3d printed holders. The holders are screwed to the ceiling (over 40 holes, this was a mass and took massive time)


## ToDo (currently not planned)
- Dimming
- Schematics
- Build instructions (video)
- (Code) documentation
- Smart Home integration (Google, Amazon)
- Professional boards
- Button codes (press 2 times for ...)

## Pictures
![](img/controller.jpg)
The controller. All switches, etc are connected to it

![](img/controller_back.jpg)
The back of the controller (a cable mess)

![](img/wall_controlboard.jpg)
The lamp controller. It connects to the controller, has 8 status LEDs on it (1 power LED). It uses a shift register to address all lights

![](img/lamps_turned_off.jpg)
The ceiling with all 8 lamps (and the "default room lamp"). The cables are kind of hidden in PVC pipes.

![](img/lamps_partly_turned_on.jpg)
The lamps, one turned on, 2 off


## Last words
Feel free to make also a project, but don't overcomplicate stuff (don't drill 50 holes in your ceiling).
Maybe I'll make a version 5 (with way more documentation), but v4 works pretty well, so don't expect it soon.