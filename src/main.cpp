#include <Arduino.h>

#include <TimedAction.h>
#include <ESP8266WiFi.h>

// enter your configuration here
#define WIFI_SSID "ssid"
#define WIFI_PASSWORD "password"

#define WEB_PASSWORD "secret"

#define PIN_OUT_LATCH 4
#define PIN_OUT_CLOCK 0
#define PIN_OUT_DATA 5

#define PIN_IN_LATCH 14
#define PIN_IN_CLOCK 2
#define PIN_IN_DATA 12

#define VERSION "4.3"

bool outputStates[] = {LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW,
                       LOW}; //5x NULL;Power;Error-LED; Table-Lamp; Lamp: 1-8;
int lampIndexStart = 8;
bool switchStates[] = {LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW};
bool oldSwitchStates[] = {LOW, LOW, LOW, LOW, LOW, LOW, LOW, LOW};

bool previousWifiConnectionState = false;
uint lastConnectStart = 0;

WiFiServer server(80);

void startWebServer() {
    Serial.println("[WEB] Starting...");
    server.begin();
    Serial.println("[WEB] Started.");

}

void checkWifiConnection() {
    if (WiFi.status() == WL_CONNECTED) {
        if (!previousWifiConnectionState) { // was not connected before
            Serial.println("[WiFi] Connected!");
            Serial.print("[WiFi] My ip address is: ");
            Serial.println(WiFi.localIP());
            previousWifiConnectionState = true;
            startWebServer();
        }
        return;
    }
    if (previousWifiConnectionState) { // was connected. but not anymore
        Serial.println("[WiFi] Connection lost!");
        Serial.println("[WEB] Stopping...");
        server.stop();
        Serial.println("[WiFi] Reconnecting...");
    } else {
        Serial.print("[WiFi] Connecting to ");
        Serial.println(WIFI_SSID);
    }

    uint time = millis();
    if (time - lastConnectStart < 1000u) {
        return;
    }
    lastConnectStart = time;

    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    previousWifiConnectionState = false;
}

bool isAtLeast1LampTurnedOn() {
    for (int i = lampIndexStart; i < (lampIndexStart + 8); i++) {
        if (outputStates[i]) {
            return true;
        }
    }
    return false;

}

void shiftDataOut() {
    digitalWrite(PIN_OUT_LATCH, LOW);
    digitalWrite(PIN_OUT_DATA, LOW);
    digitalWrite(PIN_OUT_CLOCK, LOW);

    for (bool outputState : outputStates) {
        digitalWrite(PIN_OUT_CLOCK, LOW);
        digitalWrite(PIN_OUT_DATA, outputState);
        digitalWrite(PIN_OUT_CLOCK, HIGH);

    }
    digitalWrite(PIN_OUT_LATCH, HIGH);
}


String split(String &string, char parser, int index) {
    // thanks google
    String rs = "";
    int parserCnt = 0;
    int rFromIndex, rToIndex = -1;
    while (index >= parserCnt) {
        rFromIndex = rToIndex + 1;
        rToIndex = string.indexOf(parser, rFromIndex);
        if (index == parserCnt) {
            if (rToIndex == 0 || rToIndex == -1) return "";
            return string.substring(rFromIndex, rToIndex);
        } else parserCnt++;
    }
    return rs;
}

bool stringToBool(String &string) {
    if (string == "an" || string == "1" || string == "true" || string == "HIGH") {
        return true;
    }
    return false;
}

void setPowerSupplyState(boolean state) {
    outputStates[5] = state;
    shiftDataOut();
    Serial.printf("Switching Power supply: %s\n", ((state) ? "on" : "off"));
}

void checkPowerSupplyState() {
    setPowerSupplyState(isAtLeast1LampTurnedOn());
}

void setLampState(int lamp, bool state, int device) {
    switch (device) {
        case 1:
            //Switch
            Serial.print("[SWITCH] ");
            break;
        case 2:
            //Web
            Serial.print("[WEB] ");
            break;
        default:
            return;
    }

    outputStates[lampIndexStart + lamp] = state;

    Serial.printf("#%d: %s\n", lamp, ((state) ? "on" : "off"));
    setPowerSupplyState(isAtLeast1LampTurnedOn());
    shiftDataOut();
}

void checkForWebClient() {
    WiFiClient client = server.available();
    if (!client) {
        return;
    }
    Serial.println("[WEB] Handling new client!");

    ulong timeout = millis() + 100;

    while (!client.available() && millis() < timeout) {
        yield();
    }
    if (!client.available() && millis() >= timeout) {
        Serial.println("[WEB] Client timed out!");
        return;
    }

    String sRequest = client.readStringUntil('\r');
    client.flush();


    if (sRequest == "") {
        Serial.println("[WEB] Empty request!");
        client.stop();
        return;
    }


    String sResponse, sHeader;

    sRequest = split(sRequest, ' ', 1);
    String data[3]; // password, lamp, state
    data[0] = split(sRequest, '/', 1);
    data[1] = split(sRequest, '/', 2);
    data[2] = split(sRequest, '/', 3);

    if (data[0] != WEB_PASSWORD) {
        // invalid password, kick this "hacker"
        sHeader = "HTTP/1.1 403 Forbidden\r\n";
        sHeader += "Content-Type: text/html\r\n";
        client.print(sHeader);
        client.println();
        client.println("Invalid credentials or usage");
        client.print("Usage: /password/lamp/state");
        client.stop();
        Serial.println("[WEB] Disconnected unknown client!");
        return;
    }

    int lampId = data[1].toInt();

    if(data[2].isEmpty()) {
        sResponse = outputStates[lampIndexStart + lampId] ? "true" : "false";
    } else {
        setLampState(lampId, stringToBool(data[2]), 2);
        sResponse = "OK";
    }


    sHeader = "HTTP/1.1 200 OK\r\n";
    sHeader += "Content-Type: text/html\r\n";

    client.print(sHeader);
    client.println();
    client.print(sResponse);

    // and stop the client
    ///client.stop();
    Serial.println("[WEB] Client disconnected.");
}


void readSwitchStates() {
    digitalWrite(PIN_IN_LATCH, 1);
    delayMicroseconds(20);
    digitalWrite(PIN_IN_LATCH, 0);

    for (int i = 0; i < 6; i++) {
        digitalWrite(PIN_IN_CLOCK, LOW);
        delayMicroseconds(2);
        switchStates[i] = digitalRead(PIN_IN_DATA);
        digitalWrite(PIN_IN_CLOCK, HIGH);
    }
}

void turnOffAllLampsSilent() {
    // does not shift out
    for (int i = lampIndexStart; i < (lampIndexStart + 8); i++) {
        outputStates[i] = LOW;
    }
}

void setSwitchState(int type) {
    Serial.print("[SWITCH] ");

    switch (type) {
        case 0: {
            Serial.print(" All lamps");
            bool nextAction = isAtLeast1LampTurnedOn();
            for (int i = lampIndexStart; i < (lampIndexStart + 8); i++) {
                outputStates[i] = !nextAction;
            }
            Serial.printf(" %s", (nextAction ? "on" : "off"));
        }
            break;
        case 1:
            Serial.print(" Table lamp:");
            outputStates[7] = !outputStates[7];
            Serial.printf(" %s", (outputStates[7] ? "on" : "off"));
            break;
        case 2: {
            Serial.print(" Edge lamps");
            // ToDo: this is really ugly
            bool alreadyTurnedOn =
                    outputStates[lampIndexStart] && outputStates[lampIndexStart + 2] &&
                    outputStates[lampIndexStart + 5] &&
                    outputStates[lampIndexStart + 7] && !outputStates[lampIndexStart + 1] &&
                    !outputStates[lampIndexStart + 3] &&
                    !outputStates[lampIndexStart + 4] && !outputStates[lampIndexStart + 6];

            turnOffAllLampsSilent();
            if (!alreadyTurnedOn) {
                outputStates[lampIndexStart] =
                outputStates[lampIndexStart + 2] =
                outputStates[lampIndexStart + 5] =
                outputStates[lampIndexStart + 7] = HIGH;
                Serial.print(" on");
            } else {
                Serial.print(" off");
            }
            break;
        }
        case 5: {
            Serial.print(" Inner lamps");
            bool alreadyTurnedOn =
                    !outputStates[lampIndexStart] && !outputStates[lampIndexStart + 2] &&
                    !outputStates[lampIndexStart + 5] &&
                    !outputStates[lampIndexStart + 7] && outputStates[lampIndexStart + 1] &&
                    outputStates[lampIndexStart + 3] &&
                    outputStates[lampIndexStart + 4] && outputStates[lampIndexStart + 6];

            turnOffAllLampsSilent();
            if (!alreadyTurnedOn) {
                outputStates[lampIndexStart + 1] =
                outputStates[lampIndexStart + 3] =
                outputStates[lampIndexStart + 4] =
                outputStates[lampIndexStart + 6] = HIGH;
                Serial.print(" on");
            } else {
                Serial.print(" off");
            }
        }
            break;
        case 4: {
            Serial.print(" Table lamps");
            bool alreadyTurnedOn =
                    !outputStates[lampIndexStart] && !outputStates[lampIndexStart + 3] &&
                    !outputStates[lampIndexStart + 5] &&
                    !outputStates[lampIndexStart + 6] && !outputStates[lampIndexStart + 7] &&
                    outputStates[lampIndexStart + 1] &&
                    outputStates[lampIndexStart + 2] && outputStates[lampIndexStart + 4];
            turnOffAllLampsSilent();
            if (!alreadyTurnedOn) {
                outputStates[lampIndexStart + 1] =
                outputStates[lampIndexStart + 2] =
                outputStates[lampIndexStart + 4] = HIGH;
                Serial.print(" on");
            } else {
                Serial.print(" off");
            }
        }
            break;
        case 3: {
            Serial.print(" Bed lamps");
            bool alreadyTurnedOn =
                    !outputStates[lampIndexStart] && !outputStates[lampIndexStart + 1] &&
                    !outputStates[lampIndexStart + 2] &&
                    !outputStates[lampIndexStart + 4] && !outputStates[lampIndexStart + 7] &&
                    outputStates[lampIndexStart + 3] &&
                    outputStates[lampIndexStart + 5] && outputStates[lampIndexStart + 6];
            turnOffAllLampsSilent();
            if (!alreadyTurnedOn) {
                outputStates[lampIndexStart + 3] =
                outputStates[lampIndexStart + 5] =
                outputStates[lampIndexStart + 6] = HIGH;
                Serial.print(" on");
            } else {
                Serial.print(" off");
            }
        }
            break;
    }
    Serial.println();
    checkPowerSupplyState();
    shiftDataOut();
}

void checkSwitches() {
    for (int i = 0; i < 6; i++) {
        oldSwitchStates[i] = switchStates[i];
    }
    readSwitchStates();
    for (int i = 0; i < 6; i++) {
        if (oldSwitchStates[i] != switchStates[i] && switchStates[i]) {
            setSwitchState(i);
        }
    }
}

void selfTest() {
    for (int i = 0; i < 16; i++) {
        Serial.print("Check: Lamp #");
        Serial.print(i + 1);
        Serial.println(";");
        outputStates[i] = HIGH;
        shiftDataOut();
        delay(800);
        outputStates[i] = LOW;
    }
    shiftDataOut();
}

TimedAction buttons = TimedAction(50, checkSwitches);
TimedAction wifi = TimedAction(10, checkWifiConnection);
TimedAction web = TimedAction(50, checkForWebClient);


void setup() {
    Serial.begin(115200);
    Serial.printf("Starting up... (v%s)\n", VERSION);
    pinMode(PIN_OUT_LATCH, OUTPUT);
    pinMode(PIN_OUT_CLOCK, OUTPUT);
    pinMode(PIN_OUT_DATA, OUTPUT);

    pinMode(PIN_IN_LATCH, OUTPUT);
    pinMode(PIN_IN_CLOCK, OUTPUT);
    pinMode(PIN_IN_DATA, INPUT);

    selfTest();
    checkWifiConnection();
}

void loop() {
    buttons.check();
    wifi.check();
    web.check();
}